
irclog/injest
-   [ ] Parse timestamp first, separately.
    -   rest, err := ParseTimestamp(text, TimestampStyle1)
-   [ ] Nickname cache & lookup by ID.
-   [ ] Common interface for log entry objects.
-   [ ] Factor out common "test fixtures" using log entry interface.

Shrink amount of C code.
-   [ ] Route all commands through a single golang handler func.

Example to-do section.
-   [x] Done item.
-   [ ] Not done item.
