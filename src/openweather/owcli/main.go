package main

import (
	"fmt"
	"os"
	"openweather"
	"strconv"
)

func die(format string, a ...interface{}) {
  fmt.Fprintf(os.Stderr, format + "\n", a...)
  os.Exit(1)
}

func main() {
  if len(os.Args) < 2 {
    die("Usage: %s <zipcode> [country-code]", os.Args[0])
  }

  cc := "US"
  if len(os.Args) > 2 {
    cc = os.Args[2]
  }

  zip, err := strconv.Atoi(os.Args[1])
  if err != nil {
    die("ERROR: Invalid zip code %#v", os.Args[1])
  }

  if len(cc) != 2 {
    die("ERROR: Invalid country code %#v (must be 2 chars long).", os.Args[2])
  }

  apiKey := os.Getenv(openweather.API_KEY_ENV)
  if len(apiKey) != 32 {
    die("ERROR: Environment variable %#v must be set and be 32 chars long.", openweather.API_KEY_ENV)
  }

  f := openweather.Fetcher{
    Host       : openweather.Host,
    Uri        : openweather.Uri,
    ApiKey     : apiKey,
    ZipCode    : zip,
    CountryCode: cc,
  }

  var result *openweather.Result
  if result, err = f.Fetch(); err != nil {
    die("ERROR: f.Fetch: %s", err)
  }

  fmt.Printf("%s\n", result.String())
}
