# A simple CLI for openweather.org

Example:

```
$ ./bin/build.sh ./owcli
$ $GOPATH/bin/owcli 95123 US
50° (feels 47°) clear sky in San Jose (ICBM: 37.245800,-121.830600)
```
