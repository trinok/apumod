package openweather

import (
  "bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
  "text/template"
)

const Host = "api.openweathermap.org"
const Uri = "/data/2.5/weather"
const TmplFmt = `https://{{.Host}}{{.Uri}}?zip={{printf "%05d" .ZipCode}},{{.CountryCode}}&appid={{.ApiKey}}`

var Tmpl = template.Must(template.New("OpenWeatherURL").Parse(TmplFmt))

const API_KEY_ENV = "OPENWEATHER_API_KEY"

// $ curl -sqN "https://api.openweathermap.org/data/2.5/weather?zip=95123,US&appid=$my_key" | aeson-pretty
// {
//     "wind": {
//         "deg": 330,
//         "speed": 5.14
//     },
//     "base": "stations",
//     "coord": {
//         "lat": 37.2458,
//         "lon": -121.8306
//     },
//     "cod": 200,
//     "dt": 1618203423,
//     "main": {
//         "humidity": 66,
//         "temp_min": 283.71,
//         "temp": 284.68,
//         "temp_max": 285.93,
//         "feels_like": 283.61,
//         "pressure": 1011
//     },
//     "visibility": 10000,
//     "sys": {
//         "sunset": 1618195105,
//         "country": "US",
//         "id": 5845,
//         "sunrise": 1618148258,
//         "type": 1
//     },
//     "name": "San Jose",
//     "clouds": {
//         "all": 40
//     },
//     "id": 0,
//     "weather": [
//         {
//             "icon": "03n",
//             "main": "Clouds",
//             "id": 802,
//             "description": "scattered clouds"
//         }
//     ],
//     "timezone": -25200
// }
type Result struct {
  Name     string `json:"name"`
  TimeZone float64 `json:"timezone"`

  Coord struct {
    Long  float64 `json:"lon"`
    Lat   float64 `json:"lat"`
  } `json:"coord"`

  Main struct {
    Humidity            float64 `json:"humidity"`
    Pressure            float64 `json:"pressure"`
    TempKelvin          float64 `json:"temp"`
    TempMinKelvin       float64 `json:"temp_min"`
    TempMaxKelvin       float64 `json:"temp_max"`
    TempFeelsLikeKelvin float64 `json:"feels_like"`
	} `json:"main"`

  Weather []struct {
    Desc string `json:"description"`
  } `json:"weather"`
}

func kelvinToDegF(k float64) float64 {
  return (k - 273.15) * 1.8 + 32
}

func (r Result) String() string {
  f := kelvinToDegF(r.Main.TempKelvin)
  feelsF := kelvinToDegF(r.Main.TempFeelsLikeKelvin)

  var descs []string
  for _, w := range r.Weather {
    descs = append(descs, w.Desc)
  }

  return fmt.Sprintf("%.0f° (feels %.0f°) %s in %s (ICBM: %.6f,%.6f)", f, feelsF, strings.Join(descs, ", "), r.Name, r.Coord.Lat, r.Coord.Long)
}

type Fetcher struct {
  Host         string
  Uri          string
  ApiKey       string
  ZipCode      int
  CountryCode  string
}

func (f *Fetcher) Fetch() (*Result, error) {
  url := &strings.Builder{}
  if err := Tmpl.Execute(url, f); err != nil {
    return nil, fmt.Errorf("tmpl.Execute: %s", err)
  }

  resp, err := http.Get(url.String())
	if err != nil {
    return nil, fmt.Errorf("http.Get: %s", err)
	}

	respStr := &strings.Builder{}
	if _, err := io.Copy(respStr, resp.Body); err != nil {
    return nil, fmt.Errorf("io.Copy: %s", err)
	}

	//fmt.Printf("----------BEGIN RESPONSE----------\n%s\n----------END RESPONSE----------\n", respStr)

  respReader := bytes.NewReader([]byte(respStr.String()))

  dataObj := &Result{}
	if err := json.NewDecoder(respReader).Decode(dataObj); err != nil {
    return nil, fmt.Errorf("json.NewDecoder: %s", err)
	}

  return dataObj, nil
}
