package injest

import (
  "regexp"
  "fmt"
  "strings"
)

type NickChanMode uint64

const (
  Voice  NickChanMode = 1 << iota  // '<+Nick>'
  Op                               // '<@Nick>'
  Owner                            // '<~Nick>'
  Admin                            // ChanServ sets mode: +a Bob
)

type NickInChanModeChange struct {
  Nick     NameID
  Mode     NickChanMode
  Removed  bool  // If false, `Mode` was added.
}

// Mode of nickname within the channel changed.
type NickChanModeChange struct {
  By       NameID
  Changes  []NickInChanModeChange
}

func (c *NickChanModeChange) Reset() {
  *c = NickChanModeChange{}
}

var nickChanModeRegexpStrs = []string{
  // [02:41 23/04/2005] >>> ChanServ sets mode: +oa Bob Bob
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] >>> (?P<by>\S+) sets mode: (?P<modes>[+-][a-zA-Z+-]+) (?P<nicks>.*)$`,
}

var nickChanModeRegexps []*regexp.Regexp

func init() {
  for _, reStr := range nickChanModeRegexpStrs {
    nickChanModeRegexps = append(nickChanModeRegexps, regexp.MustCompile(reStr))
  }
}

// It is assumed that line is populated with the Log ID and the line number.
func ParseNickChanModeChange(line *LogLine, text string, c *NickChanModeChange, resultErr *error) bool {
  for _, re := range nickChanModeRegexps {
    match := re.FindStringSubmatchIndex(text)
    if match == nil {
      continue
    }

    var err error
    if line.Time, err = ParseTimestamp1(text, re, match); err != nil {
      *resultErr = fmt.Errorf("ParseTimestamp1 failed: %s", err)
      return false
    }

    groupNum := re.SubexpIndex("by")
    i := match[groupNum*2]
    j := match[groupNum*2 + 1]
    // TODO: Look up ID for "by" nick.
    c.By = FakeNickIDChanServ

    groupNum = re.SubexpIndex("modes")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    c.Changes, err = parseNickChanModeList(text[i:j])
    if err != nil {
      *resultErr = fmt.Errorf("parseNickChanModeList failed: %s", err)
      return false
    }

    groupNum = re.SubexpIndex("nicks")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    if err := parseNickChanNickList(text[i:j], c.Changes); err != nil {
      *resultErr = fmt.Errorf("parseNickChanNickList failed: %s", err)
      return false
    }
    return true
  }
  return false
}

func parseNickChanModeList(modes string) ([]NickInChanModeChange, error) {
  var result []NickInChanModeChange
  gotPlusOrMinus := false
  removed := false
  for _, char := range strings.Split(modes, "") {
    switch char {
    case "+":
      removed = false
      gotPlusOrMinus = true
    case "-":
      removed = true
      gotPlusOrMinus = true
    default:
      if !gotPlusOrMinus {
        return nil, fmt.Errorf("got mode char '%s' before plus or minus", char)
      }

      mode := NickChanMode(0)
      switch char {
      case "v":
        mode = Voice
      case "o":
        mode = Op
      case "a":
        mode = Admin
      default:
        return nil, fmt.Errorf("unhandled nick channel mode char '%s'", char)
      }
      result = append(result, NickInChanModeChange{0, mode, removed})
    }
  }
  return result, nil
}

func parseNickChanNickList(nickList string, changes []NickInChanModeChange) error {
  nicks := strings.Split(strings.TrimSpace(nickList), " ")
  if len(nicks) != len(changes) {
    return fmt.Errorf("number of nicks (%d) doesn't match number of mode changes (%d)", len(nicks), len(changes))
  }

  for i, _ := range nicks {
    // TODO: s/_/nick/ and look up ID for `nick`.
    changes[i].Nick = FakeNickIDBob
  }
  return nil
}
