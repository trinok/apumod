package injest

import (
  "fmt"
  "testing"
  "time"
)

type parseMsgCase struct {
  wantLine LogLine
  text     string
  wantRet  bool
  wantErr  error
  want     Msg
}

func TestParseMsg(t *testing.T) {
  logID := LogID(1)
  lineNum := LineNum(123)
  aliceID := FakeNickIDAlice
  channelID := FakeChanID
  cases := []*parseMsgCase{
    {
      text: "[01:02 23/04/2005] <@Alice> hi lamers",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 1, 2, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: Msg{
        From: aliceID,
        To: channelID,
        Text: "hi lamers",
      },
    },
  }

  badTime := Timestamp(1000212400)
  got := Msg{}
  gotLine := LogLine{}
  err := error(nil)
  for _, c := range cases {
    err = nil
    gotLine = LogLine{logID, lineNum, badTime}
    ret := ParseMsg(&gotLine, c.text, &got, &err)
    if ret != c.wantRet {
      t.Fatalf("ParseMsg: Wrong return value: want %t, got %t & err = %v from text %q", c.wantRet, ret, err, c.text)
    }

    if c.wantErr == nil && err != nil {
      t.Fatalf("ParseMsg: Want no error, but got %v from text %q", err, c.text)
    }

    if c.wantErr != nil && err == nil {
      t.Fatalf("ParseMsg: Want error %v, but got none from text %q", c.wantErr, c.text)
    }

    if c.wantErr != nil {
      if c.wantErr.Error() != err.Error() {
        t.Fatalf("ParseMsg: Wrong error: want %v, got %v from text %q", c.wantErr, err, c.text)
      }
      continue
    }

    wantStr := fmt.Sprintf("%#v", c.wantLine)
    gotStr := fmt.Sprintf("%#v", gotLine)
    if wantStr != gotStr {
      t.Fatalf("ParseMsg (LogLine):\nwant: %s\ngot:  %s", wantStr, gotStr)
    }

    wantStr = fmt.Sprintf("%#v", c.want)
    gotStr = fmt.Sprintf("%#v", got)
    if wantStr != gotStr {
      t.Fatalf("ParseMsg:\nwant: %s\ngot:  %s", wantStr, gotStr)
    }
  }
}
