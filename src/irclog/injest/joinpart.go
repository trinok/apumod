package injest

import (
  "fmt"
  "regexp"
)

type JoinPartType uint8

const (
  Join JoinPartType = 1 + iota
  Part
  Quit
)

// Someone joined or left the channel.
type JoinPart struct {
  Nick     string
  User     User
  Which    JoinPartType
  QuitMsg  string  // Set when `Which == Quit`.
}

var joinPartRegexpStrs = []string{
  // [00:23 23/04/2005] >>> [Join] Bob (~bob@foo.example.com)
  // [00:23 23/04/2005] >>> [Part] Bob (~bob@foo.example.com)
  // [00:23 23/04/2005] >>> [Quit] Bob (~bob@foo.example.com): Quit: bye
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] >>> \[(?P<which>Join|Part|Quit)\] (?P<nick>\S+) \((?P<tilde>[~])?(?P<user>[^@]+)@(?P<host>[^)]+)\)(?:: (?P<quit_msg>.*))?$`,
}

var joinPartRegexps []*regexp.Regexp

func init() {
  for _, reStr := range joinPartRegexpStrs {
    joinPartRegexps = append(joinPartRegexps, regexp.MustCompile(reStr))
  }
}

// It is assumed that line is populated with the Log ID and the line number.
func ParseJoinPart(line *LogLine, text string, jp *JoinPart, resultErr *error) bool {
  for _, re := range joinPartRegexps {
    match := re.FindStringSubmatchIndex(text)
    if match == nil {
      continue
    }

    var err error
    if line.Time, err = ParseTimestamp1(text, re, match); err != nil {
      *resultErr = fmt.Errorf("ParseTimestamp1 failed: %s", err)
      return false
    }

    groupNum := re.SubexpIndex("which")
    i := match[groupNum*2]
    j := match[groupNum*2 + 1]
    whichStr := text[i:j]
    switch whichStr {
    case "Join":
      jp.Which = Join
    case "Part":
      jp.Which = Part
    case "Quit":
      jp.Which = Quit
    default:
      *resultErr = fmt.Errorf("ParseJoinPart: unhandled 'which' value: %q", whichStr)
      return false
    }

    groupNum = re.SubexpIndex("nick")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    jp.Nick = text[i:j]

    groupNum = re.SubexpIndex("tilde")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    if i == -1 || j == -1 {
      jp.User.Stat = HasIdentd
    }

    groupNum = re.SubexpIndex("user")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    // TODO: User ID creation or lookup.
    jp.User.ID = FakeUserIDBob
    jp.User.User = text[i:j]

    groupNum = re.SubexpIndex("host")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    jp.User.Host = text[i:j]

    groupNum = re.SubexpIndex("quit_msg")
    i = match[groupNum*2]
    j = match[groupNum*2 + 1]
    if i >= 0 {
      jp.QuitMsg = text[i:j]
    }
    return true
  }
  return false
}

func (j *JoinPart) Reset() {
  *j = JoinPart{}
}
