package injest

// Events are not log entries themselves, rather they're generated when certain
// log entries are seen.

type NickSeenType uint8

const (
  NickJoin  NickSeenType = 1 + iota
  NickPart
  NickChangeFrom  // Nick changed from this nick.
  NickChangeTo    // Nick changed to this nick.
  NickMode        // Seen in `/mode #chan <nick> ...` result.
  NickWho         // Seen in `/who #chan` result.
  NickMsg         // Saw this nick first via a message.
)

// A new nickname was discovered somehow.
type NickSeenEvent struct {
  Time  Timestamp
  User  UserID
  Nick  NameID
  How   NickSeenType
}
