package injest

import (
  "fmt"
  "testing"
  "time"
)

type parseJoinPartCase struct {
  wantLine LogLine
  text     string
  wantRet  bool
  wantErr  error
  want     JoinPart
}

func TestJoinPart(t *testing.T) {
  logID := LogID(1)
  lineNum := LineNum(123)
  cases := []*parseJoinPartCase{
    {
      text: "[00:23 23/04/2005] >>> [Join] Bob (bob@foo.example.com)",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 0, 23, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: JoinPart{
        Nick: "Bob",
        User: User{
          ID: FakeUserIDBob,
          User: "bob",
          Stat: HasIdentd,
          Host: "foo.example.com",
        },
        Which: Join,
      },
    },
    {
      text: "[00:23 23/04/2005] >>> [Part] Bob (bob@foo.example.com)",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 0, 23, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: JoinPart{
        Nick: "Bob",
        User: User{
          ID: FakeUserIDBob,
          User: "bob",
          Stat: HasIdentd,
          Host: "foo.example.com",
        },
        Which: Part,
      },
    },
    {
      text: "[00:23 23/04/2005] >>> [Quit] Bob (~bob@foo.example.com): Quit: bye",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 0, 23, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: JoinPart{
        Nick: "Bob",
        User: User{
          ID: FakeUserIDBob,
          User: "bob",
          Host: "foo.example.com",
        },
        Which: Quit,
        QuitMsg: "Quit: bye",
      },
    },
  }

  badTime := Timestamp(1000212400)
  got := JoinPart{}
  gotLine := LogLine{}
  err := error(nil)
  for _, c := range cases {
    err = nil
    got.Reset()
    gotLine = LogLine{logID, lineNum, badTime}
    ret := ParseJoinPart(&gotLine, c.text, &got, &err)
    if ret != c.wantRet {
      t.Fatalf("ParseJoinPart: Wrong return value: want %t, got %t & err = %v from text %q", c.wantRet, ret, err, c.text)
    }

    if c.wantErr == nil && err != nil {
      t.Fatalf("ParseJoinPart: Want no error, but got %v from text %q", err, c.text)
    }

    if c.wantErr != nil && err == nil {
      t.Fatalf("ParseJoinPart: Want error %v, but got none from text %q", c.wantErr, c.text)
    }

    if c.wantErr != nil {
      if c.wantErr.Error() != err.Error() {
        t.Fatalf("ParseJoinPart: Wrong error: want %v, got %v from text %q", c.wantErr, err, c.text)
      }
      continue
    }

    wantStr := fmt.Sprintf("%#v", c.wantLine)
    gotStr := fmt.Sprintf("%#v", gotLine)
    if wantStr != gotStr {
      t.Fatalf("ParseJoinPart (LogLine):\nwant: %s\ngot:  %s", wantStr, gotStr)
    }

    wantStr = fmt.Sprintf("%#v", c.want)
    gotStr = fmt.Sprintf("%#v", got)
    if wantStr != gotStr {
      t.Fatalf("ParseJoinPart:\nwant: %s\ngot:  %s\nline: %s", wantStr, gotStr, c.text)
    }
  }
}
