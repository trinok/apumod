package injest

import (
  "fmt"
  "testing"
  "time"
)

type parseIgnoredCase struct {
  text         string
  wantLine     LogLine
  wantIgnored  bool
  wantErr      error
}

func TestIgnored(t *testing.T) {
  logID := LogID(1)
  lineNum := LineNum(123)
  cases := []*parseIgnoredCase{
    {
      text: "[01:31 23/04/2005] <!> You are now marked away",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 1, 31, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[08:31 24/04/2005] Disconnected",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 24, 8, 31, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[08:34 24/04/2005] Attempting to rejoin...",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 24, 8, 34, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[08:34 24/04/2005] Now talking in #friends",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 24, 8, 34, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[09:34 24/04/2005] > @Bob is away: auto-away after 60 minutes",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 24, 9, 34, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[08:34 24/04/2005] Updated internal address list",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 24, 8, 34, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[08:34 24/04/2005] Set by Bob at 4/23/2005 9:42:52 PM",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 24, 8, 34, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
    {
      text: "[01:31 23/04/2005] Pager is on",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 1, 31, 0, 0, DefaultZone).Unix()),
      },
      wantIgnored: true,
    },
  }

  badTime := Timestamp(1000212400)
  gotLine := LogLine{}
  err := error(nil)
  for _, c := range cases {
    err = nil
    gotLine = LogLine{logID, lineNum, badTime}
    gotIgnored := ParseIgnored(&gotLine, c.text, &err)
    if gotIgnored != c.wantIgnored {
      t.Fatalf("ParseIgnored: Wrong return value: want %t, got %t & err = %v from text %q", c.wantIgnored, gotIgnored, err, c.text)
    }

    if c.wantErr == nil && err != nil {
      t.Fatalf("ParseIgnored: Want no error, but got %v from text %q", err, c.text)
    }

    if c.wantErr != nil && err == nil {
      t.Fatalf("ParseIgnored: Want error %v, but got none from text %q", c.wantErr, c.text)
    }

    if c.wantErr != nil {
      if c.wantErr.Error() != err.Error() {
        t.Fatalf("ParseIgnored: Wrong error: want %v, got %v from text %q", c.wantErr, err, c.text)
      }
      continue
    }

    wantStr := fmt.Sprintf("%#v", c.wantLine)
    gotStr := fmt.Sprintf("%#v", gotLine)
    if wantStr != gotStr {
      t.Fatalf("ParseIgnored (LogLine):\nwant: %s\ngot:  %s\nline: %s", wantStr, gotStr, c.text)
    }
  }
}
