package injest

import (
  "fmt"
  "regexp"
)

// Someone used /topic.
type TopicChange struct {
  NewTopic  string

  // Who changed it? If zero, the topic was simply seen in the log.
  Who NameID
}

var topicRegexpStrs = []string{
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] >>> (?P<who>\S+) changes topic to: '(?P<new_topic>.*)'$`,
}

var topicRegexps []*regexp.Regexp

func init() {
  for _, reStr := range topicRegexpStrs {
    topicRegexps = append(topicRegexps, regexp.MustCompile(reStr))
  }
}

// It is assumed that line is populated with the Log ID and the line number.
func ParseTopicChange(line *LogLine, text string, tc *TopicChange, resultErr *error) bool {
  for _, re := range topicRegexps {
    match := re.FindStringSubmatchIndex(text)
    if match == nil {
      continue
    }

    var err error
    if line.Time, err = ParseTimestamp1(text, re, match); err != nil {
      *resultErr = fmt.Errorf("ParseTimestamp1 failed: %s", err)
      return false
    }

    topicBuf := make([]byte, 0, len(text))
    tc.NewTopic = string(re.ExpandString(topicBuf, "$new_topic", text, match))
    // TODO
    tc.Who = FakeNickIDBob
    return true
  }
  return false
}
