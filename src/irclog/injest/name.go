package injest

type NameType uint8

const (
  IsChannel NameType = 1 + iota
  IsNick
)

type NameID int64

// Channel name or nickname.
type Name struct {
  ID    NameID
  Name  string
  Type  NameType  // One of: IsChannel, IsNick.
}

const (
  // TODO: Make a name registry & lookup or create each name when it is encountered.
  FakeNickIDBob       NameID = 10
  FakeNickIDAlice     NameID = 1000
  FakeNickIDChanServ  NameID = 9999
  FakeChanID          NameID = 3
)
