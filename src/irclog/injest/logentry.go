package injest

import (
  "errors"
  "fmt"
)

type EntryType uint8

const (
  EmptyEntry  EntryType = 0 + iota
  MsgEntry
  NickChangeEntry
  TopicChangeEntry
)

type Entry struct {
  Type  EntryType
  Line  LogLine

  Msg          Msg
  TopicChange  TopicChange
  JoinPart     JoinPart
}

func (e *Entry) Reset(line string) {
  e.Type = EmptyEntry
  e.Line.Reset()

  e.Msg = Msg{}
  e.TopicChange = TopicChange{}
}

var LineIgnored = errors.New("line ignored")
var LineUnhandled = errors.New("unhandled line type")

func (e *Entry) ParseLine(text string) error {
  var err error
  switch {
  case ParseIgnored(&e.Line, text, &err):
    if err != nil {
      return fmt.Errorf("ParseIgnored: %s", err)
    }
    return LineIgnored
  case ParseTopicChange(&e.Line, text, &e.TopicChange, &err):
    if err != nil {
      return fmt.Errorf("ParseTopicChange: %s", err)
    }
    return nil
  default:
    return LineUnhandled
  }
  return nil
}
