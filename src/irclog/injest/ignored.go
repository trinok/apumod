package injest

import (
  "fmt"
  "regexp"
)

var ignoredRegexpStrs1 = []string{
  // TODO: Emit a GapEndEvent.
  // [08:34 24/04/2005] Now talking in #friends
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] Now talking in (?P<channel>#\S+)$`,

  // [01:31 23/04/2005] <!> You are now marked away
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] <!> You are now marked away$`,

  // TODO: Emit a GapStartEvent.
  // [08:31 24/04/2005] Disconnected
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] Disconnected$`,

  // [08:34 24/04/2005] Attempting to rejoin...
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] Attempting to rejoin...$`,

  // TODO: Add `Away` entry.
  // [09:34 24/04/2005] > @Bob is away: auto-away after 60 minutes
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] > (?P<mode>[+@~])?(?P<who>[^>]+) is away: (?P<away_msg>.*)$`,

  // [08:34 24/04/2005] Updated internal address list
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] Updated internal address list$`,

  // TODO: Add date to previous TopicChange entry.
  // [08:34 24/04/2005] Set by Bob at 4/23/2005 9:42:52 PM
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] Set by (?P<who>\S+) at .*$`,

  // [01:31 23/04/2005] Pager is on
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] Pager is on$`,
}

var ignoredRegexpStrs2 = []string{
  `^Session beginning (?P<hour>\d\d):(?P<minute>\d\d) \w+ (?P<day>\d\d)-(?P<month_abbrev>\w{3})-(?P<year>\d{4})$`,
}

var ignoredRegexps1 []*regexp.Regexp
var ignoredRegexps2 []*regexp.Regexp

func init() {
  for _, reStr := range ignoredRegexpStrs1 {
    ignoredRegexps1 = append(ignoredRegexps1, regexp.MustCompile(reStr))
  }
  for _, reStr := range ignoredRegexpStrs2 {
    ignoredRegexps2 = append(ignoredRegexps2, regexp.MustCompile(reStr))
  }
}

func ParseIgnored(line *LogLine, text string, resultErr *error) bool {
  for _, re := range ignoredRegexps1 {
    match := re.FindStringSubmatchIndex(text)
    if match == nil {
      continue
    }

    // If a timestamp is available on this line, return it to the caller in `line`.
    var err error
    if line.Time, err = ParseTimestamp1(text, re, match); err != nil {
      *resultErr = fmt.Errorf("ParseTimestamp1 failed: %s", err)
      return false
    }
    return true
  }

  for _, re := range ignoredRegexps2 {
    match := re.FindStringSubmatchIndex(text)
    if match == nil {
      continue
    }

    // If a timestamp is available on this line, return it to the caller in `line`.
    var err error
    if line.Time, err = ParseTimestamp2(text, re, match); err != nil {
      *resultErr = fmt.Errorf("ParseTimestamp2 failed: %s", err)
      return false
    }
    return true
  }
  return false
}
