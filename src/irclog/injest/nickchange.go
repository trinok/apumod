package injest

// Someone used /nick.
type NickChange struct {
  Line  LogLine
  From  NameID
  To    NameID
}
