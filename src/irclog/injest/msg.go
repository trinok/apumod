package injest

import (
  "fmt"
  "regexp"
)

// A message sent to a nick or channel.
type Msg struct {
  From   NameID
  To     NameID
  Text   string
}

var msgRegexpStrs = []string{
  // "[01:02 23/04/2005] <@Alice> hi lamers"
  `^\[(?P<hour>\d\d):(?P<minute>\d\d) (?P<day>\d\d)/(?P<month>\d\d)/(?P<year>\d{4})\] <(?P<mode>[+@~])?(?P<who>[^>]+)> (?P<msg>.*)$`,
}

var msgRegexps []*regexp.Regexp

func init() {
  for _, reStr := range msgRegexpStrs {
    msgRegexps = append(msgRegexps, regexp.MustCompile(reStr))
  }
}

func ParseMsg(line *LogLine, text string, msg *Msg, resultErr *error) bool {
  // TODO: Emit a NickSeenEvent w/ somehow and include user's mode in the
  // channel (op: '@', voice: '+', etc.

  for _, re := range msgRegexps {
    match := re.FindStringSubmatchIndex(text)
    if match == nil {
      continue
    }

    var err error
    if line.Time, err = ParseTimestamp1(text, re, match); err != nil {
      *resultErr = fmt.Errorf("ParseTimestamp1 failed: %s", err)
      return false
    }

    msgBuf := make([]byte, 0, len(text))
    msg.From = FakeNickIDAlice
    msg.To = FakeChanID
    msg.Text = string(re.ExpandString(msgBuf, "$msg", text, match))
    return true
  }
  return false
}
