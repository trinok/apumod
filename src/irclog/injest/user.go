package injest

type UserID     int64
type UserStatus uint64

const (
  HasIdentd  UserStatus = 1 << iota
  MaskedIP
  MaskedHost
)

// A User is identified by `<User>@<Host>` and may be associated with multiple
// nicknames via `NickEvent`.
type User struct {
  ID UserID

  // A clean identd "username" or the "username" part of "~username" when no
  // satisfactory identd response was received - the tilde is preserved as
  // "identd" in `Stat`.
  User string

  // Full hostname or IP address. Possibly masked by the IRC server for privacy.
  Host string

  // Bits: HasIdentd | MaskedIP | MaskedHost | ...
  Stat UserStatus

  // If Stat & MaskedHost, the part which wasn't masked. Optional.
  UHost  string
}

const (
  FakeUserIDBob UserID = 1010
)
