package injest

import (
  "fmt"
  "testing"
  "time"
)

type parseNickChanModeChangeCase struct {
  wantLine LogLine
  text     string
  wantRet  bool
  wantErr  error
  want     NickChanModeChange
}

func TestNickChanModeChange(t *testing.T) {
  logID := LogID(1)
  lineNum := LineNum(123)
  cases := []*parseNickChanModeChangeCase{
    {
      text: "[02:41 23/04/2005] >>> ChanServ sets mode: +oa Bob Bob",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 23, 2, 41, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: NickChanModeChange{
        By: FakeNickIDChanServ,
        Changes: []NickInChanModeChange{
          {FakeNickIDBob, Op, false},
          {FakeNickIDBob, Admin, false},
        },
      },
    },
    {
      text: "[21:30 01/04/2005] >>> ChanServ sets mode: +o Bob",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 1, 21, 30, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: NickChanModeChange{
        By: FakeNickIDChanServ,
        Changes: []NickInChanModeChange{
          {FakeNickIDBob, Op, false},
        },
      },
    },
  }

  badTime := Timestamp(1000212400)
  got := NickChanModeChange{}
  gotLine := LogLine{}
  err := error(nil)
  for _, c := range cases {
    err = nil
    got.Reset()
    gotLine = LogLine{logID, lineNum, badTime}
    ret := ParseNickChanModeChange(&gotLine, c.text, &got, &err)
    if ret != c.wantRet {
      t.Fatalf("ParseNickChanModeChange: Wrong return value: want %t, got %t & err = %v from text %q", c.wantRet, ret, err, c.text)
    }

    if c.wantErr == nil && err != nil {
      t.Fatalf("ParseNickChanModeChange: Want no error, but got %v from text %q", err, c.text)
    }

    if c.wantErr != nil && err == nil {
      t.Fatalf("ParseNickChanModeChange: Want error %v, but got none from text %q", c.wantErr, c.text)
    }

    if c.wantErr != nil {
      if c.wantErr.Error() != err.Error() {
        t.Fatalf("ParseNickChanModeChange: Wrong error: want %v, got %v from text %q", c.wantErr, err, c.text)
      }
      continue
    }

    wantStr := fmt.Sprintf("%#v", c.wantLine)
    gotStr := fmt.Sprintf("%#v", gotLine)
    if wantStr != gotStr {
      t.Fatalf("ParseNickChanModeChange (LogLine):\nwant: %s\ngot:  %s", wantStr, gotStr)
    }

    wantStr = fmt.Sprintf("%#v", c.want)
    gotStr = fmt.Sprintf("%#v", got)
    if wantStr != gotStr {
      t.Fatalf("ParseNickChanModeChange:\nwant: %s\ngot:  %s\nline: %s", wantStr, gotStr, c.text)
    }
  }
}

type parseNickChanModeListCase struct {
  modes    string
  wantErr  error
  want     []NickInChanModeChange
}

func TestParseNickChanModeList(t *testing.T) {
  cases := []*parseNickChanModeListCase{
    {
      modes: "+oa",
      want: []NickInChanModeChange{
        {0, Op, false},
        {0, Admin, false},
      },
    },
  }

  for _, c := range cases {
    got, err := parseNickChanModeList(c.modes)
    if c.wantErr == nil && err != nil {
      t.Fatalf("parseNickChanModeList: Want no error, but got %v from modes %q", err, c.modes)
    }

    if c.wantErr != nil && err == nil {
      t.Fatalf("parseNickChanModeList: Want error %v, but got none from modes %q", c.wantErr, c.modes)
    }

    if c.wantErr != nil {
      if c.wantErr.Error() != err.Error() {
        t.Fatalf("parseNickChanModeList: Wrong error: want %v, got %v from modes %q", c.wantErr, err, c.modes)
      }
      continue
    }

    wantStr := fmt.Sprintf("%#v", c.want)
    gotStr := fmt.Sprintf("%#v", got)
    if wantStr != gotStr {
      t.Fatalf("parseNickChanModeList:\nwant: %s\ngot:  %s\nmodes: %s", wantStr, gotStr, c.modes)
    }
  }
}
