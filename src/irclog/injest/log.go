package injest

import (
  "fmt"
  "regexp"
  "time"
)

type LogID      int64
type LineNum    int64
type Timestamp  int64

type Log struct {
  ID    LogID
  Name  string  // Filename.
}

type LogLine struct {
  Log   LogID
  Num   LineNum
  Time  Timestamp  // Zero if unknown.
}

func (l *LogLine) Reset() {
  l.Log = 0
  l.Num = 0
  l.Time = 0
}

var DefaultZone *time.Location

func init() {
  // TODO: Make a timezone flag or something.
  DefaultZone = time.FixedZone("EDT", -4 * 60)
}

const tsLayout1 = "2006/01/02 15:04:05"

// Parse timestamps such as "19:56 25/04/2005"
func ParseTimestamp1(text string, re *regexp.Regexp, match []int) (Timestamp, error) {
  tsBuf := make([]byte, 0, len(tsLayout1))
  tsBuf = re.ExpandString(tsBuf, "$year/$month/$day $hour:$minute:00", text, match)
  ts, err := time.ParseInLocation(tsLayout1, string(tsBuf), DefaultZone)
  if err != nil {
    return 0, fmt.Errorf("time.Parse failed: %s", err)
  }
  return Timestamp(ts.Unix()), nil
}

const tsLayout2 = "02-Jan-2006 15:04:05"

// Parse timestamps such as "00:00 Saturday 23-Apr-2005"
func ParseTimestamp2(text string, re *regexp.Regexp, match []int) (Timestamp, error) {
  tsBuf := make([]byte, 0, len(tsLayout2))
  tsBuf = re.ExpandString(tsBuf, "$day-$month_abbrev-$year $hour:$minute:00", text, match)

  ts, err := time.ParseInLocation(tsLayout2, string(tsBuf), DefaultZone)
  if err != nil {
    return 0, fmt.Errorf("time.Parse failed: %s", err)
  }
  return Timestamp(ts.Unix()), nil
}
