package injest

import (
  "fmt"
  "testing"
  "time"
)

type parseTopicChangeCase struct {
  wantLine LogLine
  text     string
  wantRet  bool
  wantErr  error
  want     TopicChange
}

func TestTopicChange(t *testing.T) {
  logID := LogID(1)
  lineNum := LineNum(123)
  bobID := FakeNickIDBob
  cases := []*parseTopicChangeCase{
    {
      text: "[19:56 25/04/2005] >>> Bob changes topic to: 'Friends' friends chat \"free\": https://www.example.com/'",
      wantLine: LogLine{
        Log: logID,
        Num: lineNum,
        Time: Timestamp(time.Date(2005, 4, 25, 19, 56, 0, 0, DefaultZone).Unix()),
      },
      wantRet: true,
      want: TopicChange{
        NewTopic: "Friends' friends chat \"free\": https://www.example.com/",
        Who: bobID,
      },
    },
  }

  badTime := Timestamp(1000212400)
  got := TopicChange{}
  gotLine := LogLine{}
  err := error(nil)
  for _, c := range cases {
    err = nil
    gotLine = LogLine{logID, lineNum, badTime}
    ret := ParseTopicChange(&gotLine, c.text, &got, &err)
    if ret != c.wantRet {
      t.Fatalf("ParseTopicChange: Wrong return value: want %t, got %t & err = %v from text %q", c.wantRet, ret, err, c.text)
    }

    if c.wantErr == nil && err != nil {
      t.Fatalf("ParseTopicChange: Want no error, but got %v from text %q", err, c.text)
    }

    if c.wantErr != nil && err == nil {
      t.Fatalf("ParseTopicChange: Want error %v, but got none from text %q", c.wantErr, c.text)
    }

    if c.wantErr != nil {
      if c.wantErr.Error() != err.Error() {
        t.Fatalf("ParseTopicChange: Wrong error: want %v, got %v from text %q", c.wantErr, err, c.text)
      }
      continue
    }

    wantStr := fmt.Sprintf("%#v", c.wantLine)
    gotStr := fmt.Sprintf("%#v", gotLine)
    if wantStr != gotStr {
      t.Fatalf("ParseTopicChange (LogLine):\nwant: %s\ngot:  %s", wantStr, gotStr)
    }

    wantStr = fmt.Sprintf("%#v", c.want)
    gotStr = fmt.Sprintf("%#v", got)
    if wantStr != gotStr {
      t.Fatalf("ParseTopicChange:\nwant: %s\ngot:  %s", wantStr, gotStr)
    }
  }
}
