// Package injest reads one or more IRC log files and converts them into a
// single stream of golang objects in memory. The intention is for another
// package (e.g: an sqlite table writer) to import this one.
package injest

import (
  "fmt"
  "os"
)

type Reader struct {
  filename  string
  file      *os.File
}

func NewReader(filename string) (*Reader, error) {
  f, err := os.Open(filename)
  if err != nil {
    return nil, fmt.Errorf("os.Open: %s", err)
  }
  return &Reader{filename, f}, nil
}

func (r *Reader) Next() (*Entry, error) {
  // TODO
  return &Entry{Type: EmptyEntry}, nil
}
