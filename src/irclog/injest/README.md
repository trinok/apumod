# irclog/injest - An IRC log file reader.

Package `irclog/injest` reads a pre-sorted series of IRC log files and converts
them into a single stream of golang objects. The intention is for another
package (e.g: an sqlite table writer) to make use of it.
