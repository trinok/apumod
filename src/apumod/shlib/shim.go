package main

// #define MODULE_NAME "apu"
// #define MAKING_MODS
// #define MAKING_APU
//
// #include <stdio.h>
// #include <tcl8.6/tk.h>
// #include <config.h>
// #include <eggdrop.h>
// #include <main.h>
// #include <cmdt.h>
// #include <mod/module.h>
// #include <users.h>
// #include <chan.h>
// #include <shlib_utils.h>
// #include <shlib_global.h>
//
// // Avoid this warning (including proto.h results in a broken macro):
// //   warning: implicit declaration of function 'n_malloc'; did you mean
// //   'nmalloc'? [-Wimplicit-function-declaration]
// extern void *n_malloc(int, const char *, int);
//
// extern _GoString_ go_apu_start();
// extern void      go_apu_close();
// extern long long go_apu_expmem();
// extern void      go_apu_report(long long idx, long long details);
// extern void      go_apu_report(long long idx, long long details);
//
// char*         apu_start(Function* func_table);
// static char*  apu_close();
// static int    apu_expmem();
// static void   apu_report(int idx, int details);
//
// Function* global = NULL;
//
// static Function apu_table[] = {
//   (Function) apu_start,
//   (Function) apu_close,
//   (Function) apu_expmem,
//   (Function) apu_report,
// };
//
// // TODO: Move all this "hello" stuff to a separate file or package.
// extern int go_hello_chan_handler(char *nick, char *host, char *hand,
//                                 char *channel, char *text);
//
// extern int go_hello_msg_handler(char *nick, char *host,
//                                 struct userrec *u, char *text);
//
// extern int go_hello_dcc_handler(struct userrec *u, int idx, char *par);
//
// static int irc_hello_setup(char *mod);
//
// static int server_hello_setup(char *mod);
//
// static cmd_t hello_load[] = {
//   {"server", "", server_hello_setup, NULL},
//   {"irc", "", irc_hello_setup, NULL},
//   {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t hello_pub[] = {
//     {"!hello", "",  go_hello_chan_handler, NULL},
//     {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t hello_msg[] = {
//     {"hello", "", go_hello_msg_handler, NULL},
//     {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t hello_dcc[] = {
//   {"hello", "", go_hello_dcc_handler, NULL},
//   {NULL, NULL, NULL, NULL}
// };
//
// // ------------------------------------
//
// // TODO: Move all this "weather" stuff to a separate file or package.
// extern int go_weather_chan_handler(char *nick, char *host, char *hand,
//                                    char *channel, char *text);
//
// extern int go_weather_msg_handler(char *nick, char *host,
//                                   struct userrec *u, char *text);
//
// extern int go_weather_dcc_handler(struct userrec *u, int idx, char *par);
//
// static int irc_weather_setup(char *mod);
//
// static int server_weather_setup(char *mod);
//
// static cmd_t weather_load[] = {
//   {"server", "", server_weather_setup, NULL},
//   {"irc", "", irc_weather_setup, NULL},
//   {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t weather_pub[] = {
//     {"!weather", "",  go_weather_chan_handler, NULL},
//     {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t weather_msg[] = {
//     {"weather", "", go_weather_msg_handler, NULL},
//     {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t weather_dcc[] = {
//   {"weather", "", go_weather_dcc_handler, NULL},
//   {NULL, NULL, NULL, NULL}
// };
//
// // ------------------------------------
//
// // TODO: Move all this "scammer" stuff to a separate file or package.
// extern int go_scammer_chan_handler(char *nick, char *host, char *hand,
//                                    char *channel, char *text);
//
// extern int go_scammer_msg_handler(char *nick, char *host,
//                                   struct userrec *u, char *text);
//
// extern int go_scammer_dcc_handler(struct userrec *u, int idx, char *par);
//
// static int irc_scammer_setup(char *mod);
//
// static int server_scammer_setup(char *mod);
//
// static cmd_t scammer_load[] = {
//   {"server", "", server_scammer_setup, NULL},
//   {"irc", "", irc_scammer_setup, NULL},
//   {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t scammer_pub[] = {
//     {"!scammer", "",  go_scammer_chan_handler, NULL},
//     {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t scammer_msg[] = {
//     {"scammer", "", go_scammer_msg_handler, NULL},
//     {NULL, NULL, NULL, NULL}
// };
//
// static cmd_t scammer_dcc[] = {
//   {"scammer", "", go_scammer_dcc_handler, NULL},
//   {NULL, NULL, NULL, NULL}
// };
//
// // ------------------------------------
//
// static char* apu_close() {
//   dprintf(DP_LOG, "DEBUG[LOG]: apu_close() called.\n");
//   dprintf(DP_STDOUT, "DEBUG[STDOUT]: apu_close() called.\n");
//   dprintf(DP_STDERR, "DEBUG[STDERR]: apu_close() called.\n");
//   dprintf(DP_SERVER, "PRIVMSG #apu :Hello from golang's cgo.\n");
//   go_apu_close();
//   rem_builtins(H_load, hello_load);
//   rem_builtins(H_dcc, hello_dcc);
//   rem_builtins(H_load, weather_load);
//   rem_builtins(H_dcc, weather_dcc);
//   rem_builtins(H_load, scammer_load);
//   rem_builtins(H_dcc, scammer_dcc);
//   module_undepend(MODULE_NAME);
//   return NULL;
// }
//
// static int apu_expmem() {
//   return (int)go_apu_expmem();
// }
//
// static void apu_report(int idx, int details) {
//   go_apu_report(idx, details);
// }
//
// char* apu_start(Function* func_table) {
//   global = func_table;
//   if (global == NULL) {
//     fprintf(stderr, "ERROR: apu_start: global is NULL!\n");
//   }
//
//   module_register(MODULE_NAME, apu_table, 0, 1);
//
//   _GoString_ gs = go_apu_start();
//   size_t gs_len = _GoStringLen(gs);
//   if (gs_len > 0) {
//     char* s = (char*)nmalloc(gs_len + 1);
//     memset(s, 0, gs_len + 1);
//     memcpy(s, _GoStringPtr(gs), gs_len);
//
//     go_apu_close();
//     module_undepend(MODULE_NAME);
//     return s;
//   }
//
//   add_builtins(H_load, hello_load);
//   add_builtins(H_dcc, hello_dcc);
//   server_hello_setup(NULL);
//   irc_hello_setup(NULL);
//
//   add_builtins(H_load, weather_load);
//   add_builtins(H_dcc, weather_dcc);
//   server_weather_setup(NULL);
//   irc_weather_setup(NULL);
//
//   add_builtins(H_load, scammer_load);
//   add_builtins(H_dcc, scammer_dcc);
//   server_scammer_setup(NULL);
//   irc_scammer_setup(NULL);
//
//   return NULL;
// }
//
// // -----------------------------
//
// // TODO: Move all this "hello" stuff to a separate file or package.
// static int irc_hello_setup(char *mod)
// {
//   p_tcl_bind_list H_temp;
//
//   if ((H_temp = find_bind_table("pub")))
//     add_builtins(H_temp, hello_pub);
//   return 0;
// }
//
// static int server_hello_setup(char *mod)
// {
//   p_tcl_bind_list H_temp;
//
//   if ((H_temp = find_bind_table("msg")))
//     add_builtins(H_temp, hello_msg);
//   return 0;
// }
//
// // -----------------------------
//
// // TODO: Move all this "weather" stuff to a separate file or package.
// static int irc_weather_setup(char *mod)
// {
//   p_tcl_bind_list H_temp;
//
//   if ((H_temp = find_bind_table("pub")))
//     add_builtins(H_temp, weather_pub);
//   return 0;
// }
//
// static int server_weather_setup(char *mod)
// {
//   p_tcl_bind_list H_temp;
//
//   if ((H_temp = find_bind_table("msg")))
//     add_builtins(H_temp, weather_msg);
//   return 0;
// }
//
// // -----------------------------
//
// // TODO: Move all this "scammer" stuff to a separate file or package.
// static int irc_scammer_setup(char *mod)
// {
//   p_tcl_bind_list H_temp;
//
//   if ((H_temp = find_bind_table("pub")))
//     add_builtins(H_temp, scammer_pub);
//   return 0;
// }
//
// static int server_scammer_setup(char *mod)
// {
//   p_tcl_bind_list H_temp;
//
//   if ((H_temp = find_bind_table("msg")))
//     add_builtins(H_temp, scammer_msg);
//   return 0;
// }
import "C"
