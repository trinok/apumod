package main

// #define MODULE_NAME "apu"
// #define MAKING_APU
//
// #include <stdio.h>
// #include <tcl8.6/tk.h>
// #include <config.h>
// #include <eggdrop.h>
// #include <chan.h>
// #include <main.h>
// #include <cmdt.h>
// #include <users.h>
// #include "shlib_utils.h"
import "C"

import (
  "fmt"
  "math/rand"
  "os"
  "openweather"
  "covidbailouttracker"
  "strconv"
  "strings"
  "time"
)

var log_file *os.File

func log(format string, a ...interface{}) {
  if log_file == nil {
    ts := time.Now().Format("20060102-150405.000000")
    log_name := fmt.Sprintf("%s/%s.%d", os.TempDir(), ts, os.Getpid())
    fmt.Fprintf(os.Stderr, "[DEBUG] Log: %s\n", log_name)

    var err error
    log_file, err = os.OpenFile(log_name, os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
      fmt.Fprintf(os.Stderr, "ERROR: os.Open(%q): %s\n", log_name, err)
      return
    }
  }
  fmt.Fprintf(log_file, format + "\n", a...)
  log_file.Sync()
}

// Filled in by an init() func in randphrases.go (that file is excluded from
// the git repo in .gitignore). If not filled in, the random phrase will be
// omitted. Example for randphrases.go:
//
// func init() {
//   helloPhrases = []string{
//     "Computers are neat.",
//     "I like sandwiches.",
//     "My father is a washing machine.",
//   }
// }
var helloPhrases []string

func randPhraseForHello() string {
  if len(helloPhrases) < 1 {
    return ""
  }
  return helloPhrases[rand.Intn(len(helloPhrases))]
}

//export go_apu_start
func go_apu_start() string {
  log("go_apu_start() called.")
  return ""
}

//export go_apu_close
func go_apu_close() {
  log("go_apu_close() called.")
}

//export go_apu_expmem
func go_apu_expmem() int {
  log("go_apu_expmem() called.")
  return 0
}

//export go_apu_report
func go_apu_report(idx, details int) {
  log("go_apu_report() called.")
}

//export go_main
func go_main() {
  log("go_main() called.")
}

func privMsg(to *C.char, format string, a ...interface{}) {
  msg := fmt.Sprintf("PRIVMSG " + C.GoString(to) + " : " + format + "\n", a...)
  C.call_qserver(C.DP_SERVER, C.CString(msg), C.int(len(msg)))
}

// Handles messages sent to the channel.
//
//export go_hello_chan_handler
func go_hello_chan_handler(nick, host, hand, channel, text *C.char) C.int {
  log("go_hello_chan_handler: Called. nick=%q host=%q hand=%q channel=%q text=%q", C.GoString(nick), C.GoString(host), C.GoString(hand), C.GoString(channel), C.GoString(text))

  chanset := C.call_findchan_by_dname(channel)
  if chanset == nil {
    log("go_hello_chan_handler: Can't find channel.")
    return 0
  }

  log("go_hello_chan_handler: Found channel. name=%q", C.GoString(&chanset.name[0]))

  privMsg(&chanset.name[0], "Hello from golang! %s", randPhraseForHello())
  return 0
}

// Handles messages sent directly to the bot.
//
//export go_hello_msg_handler
func go_hello_msg_handler(nick, host *C.char, u *C.struct_userrec, text *C.char) C.int {
  nick_go := "<nil>"
  if nick != nil {
    nick_go = C.GoString(nick)
  }
  host_go := "<nil>"
  if host != nil {
    host_go = C.GoString(host)
  }
  handle_go := "<nil>"
  if u != nil {
    handle_go = C.GoString(&u.handle[0])
  }
  text_go := "<nil>"
  if text != nil {
    text_go = C.GoString(text)
  }

  log("go_hello_msg_handler() called. nick=%q host=%q u.handle=%q text=%q", nick_go, host_go, handle_go, text_go)
  privMsg(nick, "Hello from golang! %s", randPhraseForHello())
  return 0
}

// Handles messages sent directly to the bot via DCC or on the console.
//
//export go_hello_dcc_handler
func go_hello_dcc_handler(u *C.struct_userrec, idx C.int, par *C.char) C.int {
  log("go_hello_dcc_handler() called. u.handle=%q idx=%d par=%q", C.GoString(&u.handle[0]), int(idx), C.GoString(par))
  msg := fmt.Sprintf("Hello from golang! %s\n", randPhraseForHello())
  C.call_qserver(idx, C.CString(msg), C.int(len(msg)))
  return 0
}

func getWeather(zipStr string) (string, error) {
  zip, err := strconv.Atoi(zipStr)
  if err != nil {
    return "", fmt.Errorf("ERROR: Invalid zip code %q", zipStr)
  }

  f := openweather.Fetcher{
    Host       : openweather.Host,
    Uri        : openweather.Uri,
    ApiKey     : "f33c869223cff489c3bcf5e7ac9c1de4",
    ZipCode    : zip,
    CountryCode: "US",
  }

  var result *openweather.Result
  if result, err = f.Fetch(); err != nil {
    return "", fmt.Errorf("ERROR: f.Fetch: %s", err)
  }

  return result.String(), nil
}

// Handles messages sent to the channel.
//
//export go_weather_chan_handler
func go_weather_chan_handler(nick, host, hand, channel, text *C.char) C.int {
  log("go_weather_chan_handler: Called. nick=%q host=%q hand=%q channel=%q text=%q", C.GoString(nick), C.GoString(host), C.GoString(hand), C.GoString(channel), C.GoString(text))

  chanset := C.call_findchan_by_dname(channel)
  if chanset == nil {
    log("go_weather_chan_handler: Can't find channel.")
    return 0
  }
  log("go_weather_chan_handler: Found channel. name=%q", C.GoString(&chanset.name[0]))

  w, err := getWeather(C.GoString(text))
  if err != nil {
    log("ERROR: getWeather failed: %s", err)
    return 0
  }

  log("go_weather_chan_handler: Found channel. name=%q", C.GoString(&chanset.name[0]))

  privMsg(&chanset.name[0], "%s", w)
  return 0
}

// Handles messages sent directly to the bot.
//
//export go_weather_msg_handler
func go_weather_msg_handler(nick, host *C.char, u *C.struct_userrec, text *C.char) C.int {
  nick_go := "<nil>"
  if nick != nil {
    nick_go = C.GoString(nick)
  }
  host_go := "<nil>"
  if host != nil {
    host_go = C.GoString(host)
  }
  handle_go := "<nil>"
  if u != nil {
    handle_go = C.GoString(&u.handle[0])
  }
  text_go := "<nil>"
  if text != nil {
    text_go = C.GoString(text)
  }

  log("go_weather_msg_handler() called. nick=%q host=%q u.handle=%q text=%q", nick_go, host_go, handle_go, text_go)

  w, err := getWeather(C.GoString(text))
  if err != nil {
    log("ERROR: getWeather failed: %s", err)
    return 0
  }

  privMsg(nick, "%s", w)
  return 0
}

// Handles messages sent directly to the bot via DCC or on the console.
//
//export go_weather_dcc_handler
func go_weather_dcc_handler(u *C.struct_userrec, idx C.int, par *C.char) C.int {
  log("go_weather_dcc_handler() called. u.handle=%q idx=%d par=%q", C.GoString(&u.handle[0]), int(idx), C.GoString(par))

  w, err := getWeather(C.GoString(par))
  if err != nil {
    log("ERROR: getWeather failed: %s", err)
    return 0
  }

  C.call_qserver(idx, C.CString(w), C.int(len(w)))
  return 0
}

//------------------------------------------------------------------------------

// Handles messages sent to the channel.
//
//export go_scammer_chan_handler
func go_scammer_chan_handler(nick, host, hand, channel, text *C.char) C.int {
  log("go_scammer_chan_handler: Called. nick=%q host=%q hand=%q channel=%q text=%q", C.GoString(nick), C.GoString(host), C.GoString(hand), C.GoString(channel), C.GoString(text))

  chanset := C.call_findchan_by_dname(channel)
  if chanset == nil {
    log("go_scammer_chan_handler: Can't find channel.")
    return 0
  }

  chanName := &chanset.name[0]
  log("go_scammer_chan_handler: Found channel. name=%q", C.GoString(chanName))

  result, err := covidbailouttracker.Search(C.GoString(text))
  if err != nil {
    log("ERROR: covidbailouttracker.Search failed: %s", err)
    return 0
  }

  for i, line := range strings.Split(result, "\n") {
    privMsg(chanName, "%s", line)

    if i > 0 {
      break
    }
  }
  return 0
}

// Handles messages sent directly to the bot.
//
//export go_scammer_msg_handler
func go_scammer_msg_handler(nick, host *C.char, u *C.struct_userrec, text *C.char) C.int {
  nick_go := "<nil>"
  if nick != nil {
    nick_go = C.GoString(nick)
  }
  host_go := "<nil>"
  if host != nil {
    host_go = C.GoString(host)
  }
  handle_go := "<nil>"
  if u != nil {
    handle_go = C.GoString(&u.handle[0])
  }
  text_go := "<nil>"
  if text != nil {
    text_go = C.GoString(text)
  }
  log("go_scammer_msg_handler() called. nick=%q host=%q u.handle=%q text=%q", nick_go, host_go, handle_go, text_go)

  if text_go == "<nil>" {
    return 0
  }

  result, err := covidbailouttracker.Search(text_go)
  if err != nil {
    log("ERROR: covidbailouttracker.Search failed: %s", err)
    return 0
  }

  for i, line := range strings.Split(result, "\n") {
    privMsg(nick, "%s", line)

    if i > 0 {
      break
    }
  }
  return 0
}

// Handles messages sent directly to the bot via DCC or on the console.
//
//export go_scammer_dcc_handler
func go_scammer_dcc_handler(u *C.struct_userrec, idx C.int, par *C.char) C.int {
  log("go_scammer_dcc_handler() called. u.handle=%q idx=%d par=%q", C.GoString(&u.handle[0]), int(idx), C.GoString(par))

  result, err := covidbailouttracker.Search(C.GoString(par))
  if err != nil {
    log("ERROR: covidbailouttracker.Search failed: %s", err)
    return 0
  }

  for i, line := range strings.Split(result, "\n") {
    msg := fmt.Sprintf("%s\n", line)
    C.call_qserver(idx, C.CString(msg), C.int(len(msg)))

    if i > 0 {
      break
    }
  }
  return 0
}
