#ifndef _SHLIB_UTILS_H
#define _SHLIB_UTILS_H

void call_qserver(int idx, char *msg, int len);

struct chanset_t* call_findchan_by_dname(char *name);

#endif  /* ifndef _SHLIB_UTILS_H */
