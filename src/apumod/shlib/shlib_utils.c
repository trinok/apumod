#define MAKING_MODS
#define MAKING_APU

#include <stdio.h>
#include <tcl8.6/tk.h>
#include <config.h>
#include <main.h>
#include <eggdrop.h>
#include <cmdt.h>
#include <mod/module.h>
#include "shlib_global.h"
#include "shlib_utils.h"

extern void (*qserver) (int, char *, int);

// Calling C function pointers directly is not supported. So this.
void call_qserver(int idx, char *msg, int len) {
  //qserver(idx, msg, len);
  dprintf(idx, "%s", msg);
}

struct chanset_t* call_findchan_by_dname(char *name) {
  if (global == NULL) {
    fprintf(stderr, "ERROR: call_findchan_by_dname: global is NULL!\n");
    return NULL;
  }
  return findchan_by_dname(name);
}
