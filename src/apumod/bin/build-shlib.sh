#!/bin/bash
#
# Build a shared library which can be loaded into eggdrop as a module after
# building the parts of eggdrop that we'll be linking in.
#

function runlog() {
  local outfile_tmpl=$1; shift
  local mod_cmd=$(echo "$@" | tr ' ' _ | sed 's@/@--@g')
  local outfile=$(echo "$outfile_tmpl" | sed "s|{}|${mod_cmd}|g")
  outfile="${outfile}.$(date '+%Y-%m-%d-%H%M%S').$$"

  ("$@"; echo $'\nEXIT STATUS:' $?) |& tee "$outfile"
  local exit_status=$(
    tail -n1 "$outfile" \
      | sed -n 's/EXIT STATUS: //p')

  if ! [[ "$exit_status" =~ ^[0-9]+$ ]]; then
    exit_status=99
  fi
  return "$exit_status"
}

function main() {
  set -eu

  . "$(dirname "$0")/lib/env.sh"

  setup_env

  j=""
  num_cpus=$(
    cat /proc/cpuinfo \
      | grep '^processor\s*:' \
      | wc -l)
  if [[ "$num_cpus" ]]; then
    j="-j${num_cpus}"
  fi

  link_objs=(
    "bg.o"
    "botcmd.o"
    "botmsg.o"
    "botnet.o"
    "chanprog.o"
    "cmds.o"
    "dcc.o"
    "dccutil.o"
    "dns.o"
    "flags.o"
    "language.o"
    "main.o"
    "match.o"
    "mem.o"
    "misc.o"
    "misc_file.o"
    "modules.o"
    "net.o"
    "rfc1459.o"
    "tcl.o"
    "tcldcc.o"
    "tclhash.o"
    "tclmisc.o"
    "tcluser.o"
    "userent.o"
    "userrec.o"
    "users.o"
  )

  cflags=(
    "-fPIC"
    "-I${GOPATH}/src/eggdrop"
    "-I/usr/include/tcl8.6"
    "-DHAVE_CONFIG_H"
    "-DCOMPILING_MEM"
  )
  export CFLAGS="${cflags[*]}"

  echo
  echo "*** Building eggdrop"
  echo
  pushd "../eggdrop"

  if ! [[ -f "config.status" ]]; then
    runlog configure.out ./configure
  fi

  if ! [[ -f "eggint.h" ]]; then
    runlog make_config.out make config
  fi

  sed -i -E 's/^CFLAGS = -g/& -fPIC/' src/Makefile
  obj_paths=$(for o in "${link_objs[@]}"; do echo "src/$o"; done)
  runlog make.out make -B $j $obj_paths

  popd

  # Insert hacky workaround for cgo build failure.
  export CC=$(readlink -f ./bin/fakecc.sh)

  cgo_cflags=(
    "-DCOMPILING_MEM"
    "-fPIC"
    "-I/usr/include/tcl8.6"
    "-I$GOPATH/src/apumod/shlib"
    "-I$GOPATH/src/eggdrop"
    "-I$GOPATH/src/eggdrop/src"
  )
  export CGO_CFLAGS="${cgo_cflags[*]}"

  cgo_ldflags=(
    "-shared"
    "-lcrypto"
    "-ldl"
    "-lm"
    "-lnsl"
    "-lpthread"
    "-ltcl8.6"
  )
  export CGO_LDFLAGS="${cgo_ldflags[*]}"

  echo
  echo "*** Building eggdrop module shared library"
  echo
  objs=$(for o in "${link_objs[@]}"; do echo -n "$GOPATH/src/eggdrop/src/$o "; done)
  if ! go install -buildmode c-shared -ldflags "main=$objs" ./shlib; then
    echo "ERROR: go install failed."
    exit 1
  fi

  echo
  echo "SUCCESS."
  echo
  ls -l --full-time "../../pkg/linux_amd64_shared/apumod/shlib.a"
  echo
}

main "$@"
