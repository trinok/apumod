#!/bin/bash
#
# Usage: ./test.sh [go_test_args]
#
# Examples:
#
#   # Run all tests.
#   ./bin/test.sh
#
#   # Run tests in package "table", and pass the -vlog flag.
#   ./bin/test.sh -v ./table -args -vlog
#
#   # Run one particular test (regex).
#   ./bin/test.sh -v ./table -run TestFooBlah
#

set -eu

. "$(dirname "$0")/lib/env.sh"

setup_env

# Default to running all tests.
if [ $# -lt 1 ]; then
  set -- ./...
fi

go test "$@"
