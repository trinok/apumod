#!/bin/bash

set -eu

. "$(dirname "$0")/lib/env.sh"

setup_env

go clean "$@"
