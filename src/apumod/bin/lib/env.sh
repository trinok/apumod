function setup_env() {
  local git_root=$(
    git worktree list --porcelain \
      | sed -n 's/^worktree //p')

  if ! [[ -d "$git_root" ]]; then
    echo "ERROR: Failed to find the root of the git repo. Are you in one?"
    return 1
  fi

  export GOPATH="$git_root"

  cd "$GOPATH/src/apumod"
}
