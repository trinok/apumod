#!/bin/bash
#
# A wrapper around gcc that deletes an extraneous main() function definition.
#
# This is a crazy stupid hack and I don't want it, but I haven't figured out
# another way to make it work. I don't know if this is due to a cgo bug or just
# something I'm missing. Probably the latter.
#

# If non-empty, save info to /tmp/fakecc.debug.* files.
DEBUG=no

function save_debug_info() {
  local out=$1; shift

  echo >> "$out"
  env  >> "$out"
  echo >> "$out"

  local c_files=()
  local i=1
  while [[ $# -gt 0 ]]; do
    local arg=$1; shift
    echo "\$$i = '$arg'" >> "$out"
    let i++

    if [[ "$arg" =~ *.[ch] ]]; then
      c_files+=("$arg")
    fi
  done

  if [[ "${#c_files[*]}" -gt 0 ]]; then
    echo >> "$out"
    echo >> "$out"
    echo "# C files passed as args: ${c_files[*]}" >> "$out"
    head -v -n9999 "${c_files[@]}" >> "$out"
  fi

  echo >> "$out"
  echo >> "$out"
  echo "# All files in current directory." >> "$out"
  find . -type f -print0 | xargs -r0 ls -l >> "$out"
  echo >> "$out"
  echo "# C files in current directory." >> "$out"
  find . -type f -name "*.[ch]" -print0 | xargs -r0 head -n99999 >> "$out"
  echo >> "$out"
  find . -type f -name "*prolog" -print0 | xargs -r0 head -n99999 >> "$out"
  echo >> "$out"
  find . -type f -name "*.o" -print0 | xargs -r0 nm -A >> "$out"
  echo >> "$out"
}


set -u

if [[ "$DEBUG" ]]; then
  out="/tmp/fakecc.debug.$(date '+%Y-%m-%d-%H%M%S.%N').$$"
fi

my_main=$(cat << __EOT__
#include <stdio.h>

int main() {
  printf("main() called from C.\n");
  return 0;
}
__EOT__
)

my_main=$(echo "$my_main" | tr '\n' '|' | sed 's@\\@\\\\@g; s@|@\\n@g')

bad_c="$(find . -name "_cgo_main.c" | head -n1)"
if [[ -f "$bad_c" ]]; then
  sed -i "s@.* main().*{.*}.*@@" "$bad_c"
fi

if [[ "$DEBUG" ]]; then
  out="/tmp/fakecc.debug.$(date '+%Y-%m-%d-%H%M%S.%N').$$"
  save_debug_info "$out" "$@"
fi

if [[ "$DEBUG" ]]; then
  gcc "$@" &> "$out.gcc.out"
  exit_status=$?
  cat "$out.gcc.out" | tee -a "$out"
  echo >> "$out"
  echo "EXIT STATUS: $exit_status" >> "$out"
  echo >> "$out"

  if [[ "$exit_status" -ne 0 ]]; then
    echo >&2
    cat "$out" >&2
  fi
  exit "$exit_status"
fi

gcc "$@"
