# A simple CLI for covidbailouttracker.com

It just wraps a shell script for now, because I'm lazy.

Example:

```
$ ./bin/build.sh ./cbtcli
$ $GOPATH/bin/cbtcli ya moms
$1000   https://covidbailouttracker.com/company/ya-moms-groceries-buellton-ca-93427
```
