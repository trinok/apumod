package covidbailouttracker

import (
	"fmt"
  "os/exec"
  "regexp"
)

var searchRe = regexp.MustCompile(`^[a-zA-Z0-9 -]+$`)

func Search(terms string) (string, error) {
  if !searchRe.MatchString(terms) {
    return "", fmt.Errorf("disallowed search term")
  }

  cmd := exec.Command("cbt-test2", terms)
  out, err := cmd.Output()
  if err != nil {
    return "", fmt.Errorf("cbt failed: %s", err)
  }
  return string(out), nil
}
