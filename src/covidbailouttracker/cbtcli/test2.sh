#!/bin/bash
#
# Test script for performing a Search on covidbailouttracker.com.
#
# This one shows the top 2 results, only the LoanAmount & URL.
#
# Usage: ./test2.sh <search terms>
#
# Example:
#   $ ./test2.sh google construction
#

terms="$*"

form_url="https://elastic-453hatkcsa-uc.a.run.app/elastic"

req=$'{"aggs":{"totalSum":{"sum":{"field":"LoanAmount"}},"RecordType":{"terms":{"field":"RecordType","size":4}}},"query":{"bool":{"should":[{"match_bool_prefix":{"BusinessName":{"query":"'"$terms"$'","max_expansions":100,"operator":"and"}}}],"minimum_should_match":1}},"size":10,"track_total_hits":true}'

resp=$(
  curl \
    --silent \
    --include \
    --data "$req" \
    --header 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36' \
    --header 'Accept: */*' \
    --header 'Accept-Language: en-US,en;q=0.5' \
    --header 'Accept-Encoding: gzip, deflate, br' \
    --header 'Referer: https://covidbailouttracker.com/' \
    --header 'content-type: application/json' \
    --header 'Origin: https://covidbailouttracker.com' \
    --header 'DNT: 1' \
    --header 'Connection: keep-alive' \
    --header 'TE: Trailers' \
    "$form_url")

#echo "--------------------BEGIN CURL OUTPUT--------------------"
#echo "$resp"
#echo "--------------------END CURL OUTPUT--------------------"

json=$(tail -n1 <<< "$resp")

num_results=$(sed -En 's/.*"total"\s*:\s*([0-9]+).*/\1/p' <<< "$json")

if [[ -z "$num_results" ]]; then
  echo "--------------------BEGIN CURL OUTPUT--------------------"
  echo "$resp"
  echo "--------------------END CURL OUTPUT--------------------"
  echo
  echo "ERROR: No results or an error occurred. See curl output above."
  exit 1
fi

if [[ "$num_results" -eq 0 ]]; then
  echo "FYI: Zero results." >&2
  exit 0
fi

aeson-pretty <<< "$json" \
  | awk -v path='https://tetro.net/cbt' '
      /"_source":/, /^\s+[}],/ {
      if (/"LoanAmount"/) { amt = gensub(/[,"]/, "", "g", $NF) }
        if (/"id"/) { id = gensub(/[,"]/, "", "g", $NF) }
        if (/^\s+\},/) { printf "%.0f %s/%s\n", amt, path, id }
      }' \
  | tr -d '",' \
  | sort -nr \
  | sed 's/^/$/' \
  | head -n2
