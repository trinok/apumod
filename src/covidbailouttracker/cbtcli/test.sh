#!/bin/bash
#
# Test script for performing a Search on covidbailouttracker.com.
#
# Usage: ./test.sh <search terms>
#
# Example:
#   $ ./test.sh google construction
#

set -x

terms="$*"

form_url="https://elastic-453hatkcsa-uc.a.run.app/elastic"

req=$'{"aggs":{"totalSum":{"sum":{"field":"LoanAmount"}},"RecordType":{"terms":{"field":"RecordType","size":4}}},"query":{"bool":{"should":[{"match_bool_prefix":{"BusinessName":{"query":"'"$terms"$'","max_expansions":100,"operator":"and"}}}],"minimum_should_match":1}},"size":10,"track_total_hits":true}'

resp=$(
  curl \
    --silent \
    --include \
    --data "$req" \
    --header 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36' \
    --header 'Accept: */*' \
    --header 'Accept-Language: en-US,en;q=0.5' \
    --header 'Accept-Encoding: gzip, deflate, br' \
    --header 'Referer: https://covidbailouttracker.com/' \
    --header 'content-type: application/json' \
    --header 'Origin: https://covidbailouttracker.com' \
    --header 'DNT: 1' \
    --header 'Connection: keep-alive' \
    --header 'TE: Trailers' \
    "$form_url")

echo
echo "*** Response:"
echo
echo "$resp"
