package main

import (
	"covidbailouttracker"
	"fmt"
	"os"
	"strings"
)

func die(format string, a ...interface{}) {
  fmt.Fprintf(os.Stderr, format + "\n", a...)
  os.Exit(1)
}

func main() {
  if len(os.Args) < 2 {
    die("Usage: %s <search terms>", os.Args[0])
  }

  result, err := covidbailouttracker.Search(strings.Join(os.Args[1:], " "))
  if err != nil {
    die("ERROR: Search failed: %s", err)
  }

  fmt.Printf("%s", result)
}
