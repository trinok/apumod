# apumod

A stupid IRC bot for [Eggdrop][eggheads] written in golang.

## Overview

The idea is to have a minimal C shim using [Cgo][cgo] with the rest of the code
written in golang.

### Milestones

-   v0.1 (done): Hello World. Get a -hello command working.
-   v0.2 (done): Add -weather command.
-   v0.x: TODO: Add more tiny milestones here.
-   v1.0: Move most code out into a daemon process which can be more
    quickly/easily reloaded.

## Installation

### Requirements

```
sudo apt install tk-dev

cd src
git clone https://github.com/eggheads/eggdrop.git
git checkout -b apumod v1.8.4
```

### Build

```
./bin/build-shlib.sh
```

### Install

```
cp ../../pkg/*/apumod/shlib.a /path/to/eggdrop/modules/apu.so
```


[eggheads]: https://www.eggheads.org/
[cgo]: https://golang.org/cmd/cgo/
[cgo-blog]: https://blog.golang.org/cgo
